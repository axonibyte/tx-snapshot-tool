/*
 * Copyright (c) 2019 Axonibyte Innovations, LLC. All rights reserved.
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.calebpower.demo.webstandalone.action;

/**
 * HTTP request methods.
 * 
 * @author Caleb L. Power
 */
public enum HTTPMethod {
  
  /**
   * HTTP DELETE method.
   */
  DELETE,
  
  /**
   * HTTP GET method.
   */
  GET,
  
  /**
   * HTTP PATCH method.
   */
  PATCH,
  
  /**
   * HTTP POST method.
   */
  POST,
  
  /**
   * HTTP PUT method.
   */
  PUT
  
}
